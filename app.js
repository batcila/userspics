/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: 'UsersPics.Application',

    name: 'UsersPics',

    requires: [
        // This will automatically load all classes in the UsersPics namespace
        // so that application classes do not need to require each other.
        'UsersPics.*'
    ],

    // The name of the initial view to create.
    mainView: 'UsersPics.view.main.Main'
});
