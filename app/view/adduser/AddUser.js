/**
 * Created by blabl on 4.10.2018 г..
 */
Ext.define('UsersPics.view.adduser.AddUser', {
    extend: 'Ext.form.Panel',
    id: 'add-user',
    requires: [
        'Ext.field.Email',
        'Ext.field.Text',
        'Ext.form.FieldSet',
        'UsersPics.view.adduser.AddUserController',
        'UsersPics.view.adduser.AddUserModel'
    ],
    xtype: 'adduser',
    viewModel: {
        type: 'adduser'
    },
    controller: 'adduser',
    fullscreen: true,
    items: [
        {
            xtype: 'fieldset',
            title: 'New user information',
            items: [
                {
                    xtype: 'textfield',
                    name : 'name',
                    label: 'Name'
                },
                {
                    xtype: 'textfield',
                    name : 'username',
                    label: 'User name'
                },
                {
                    xtype: 'emailfield',
                    name : 'email',
                    label: 'Email'
                },
                {
                    xtype: 'textfield',
                    name : 'phone',
                    label: 'Phone'
                },
                {
                    xtype: 'textfield',
                    name : 'address',
                    label: 'Address'
                },
                {
                    xtype: 'textfield',
                    name : 'town',
                    label: 'Town'
                },
                {
                    xtype: 'textfield',
                    name : 'companyName',
                    label: 'Company Name'
                },
                {
                    xtype: 'textfield',
                    name : 'website',
                    label: 'Web Site'
                }
            ]
        }
    ]
});