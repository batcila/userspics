/**
 * Created by blabl on 4.10.2018 г..
 */
Ext.define('UsersPics.view.pictures.PicturesController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.pictures',

    /**
     * Called when the view is created
     */
    init: function() {

    },

    onItemSelected: function (sender, record) {
        Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    }
});