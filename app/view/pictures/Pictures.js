/**
 * Created by blabl on 4.10.2018 г..
 */
Ext.define('UsersPics.view.pictures.Pictures', {
    extend: 'Ext.dataview.List',

    requires: [
        'UsersPics.store.Pictures',
        'UsersPics.view.pictures.PicturesModel',
		'UsersPics.view.pictures.PicturesController'
    ],

    xtype: 'pictures',

    viewModel: {
        type: 'pictures'
    },

    controller: 'pictures',

    title: 'Photos',

    store: {
        type: 'pictures',
        autoLoad: true,
    },

    fullscreen: true,

    itemTpl: "<img src='{url}' alt='{title}' height='100' width='100'/><div> Title: {title}</div>",
});