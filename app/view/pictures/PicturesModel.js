/**
 * Created by blabl on 4.10.2018 г..
 */
Ext.define('UsersPics.view.pictures.PicturesModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.pictures',

    stores: {
        /*
        A declaration of Ext.data.Store configurations that are first processed as binds to produce an effective
        store configuration. For example:

        users: {
            model: 'Pictures',
            autoLoad: true
        }
        */
    },

    data: {
        /* This object holds the arbitrary data that populates the ViewModel and is then available for binding. */
    }
});