/**
 * Created by blabl on 8.10.2018 г..
 */
Ext.define('UsersPics.view.showuserinfo.ShowUserInfoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.showuserinfo',

    /**
     * Called when the view is created
     */
    init: function() {
        let vm = this.getViewModel();
        // vm.set('selectedUser', JSON.stringify(this.getView().selectedUser, null, '\t'))
        vm.set('selectedUser', this.getView().selectedUser)
    }
});