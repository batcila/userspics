/**
 * Created by blabl on 8.10.2018 г..
 */
Ext.define('UsersPics.view.showuserinfo.ShowUserInfo', {
    extend: 'Ext.Container',

    requires: [
        'Ext.layout.HBox',
        'UsersPics.view.showuserinfo.ShowUserInfoController',
        'UsersPics.view.showuserinfo.ShowUserInfoModel'
    ],

    xtype: 'showuserinfo',

    viewModel: {
        type: 'showuserinfo'
    },

    controller: 'showuserinfo',

    bodyPadding: 10,
    margin: '10px',
    defaultType: 'panel',
    layout: {
        type: 'hbox',
    },
    defaults: {
        bodyPadding: 10,
        border: true,
    },

    items: [{
        extend: 'Ext.form.Panel',
        title: 'User',
        flex: 1,
        margin: '40 10 40 40',
        items: [{
            xtype: 'fieldset',
            reference: 'fieldset1',
            items: [{
                xtype: 'displayfield',
                name: 'id',
                label: 'ID',
                bind: {
                    value: '{selectedUser.id}'
                }
            },{
                xtype: 'displayfield',
                name: 'name',
                label: 'Name',
                bind: {
                    value: '{selectedUser.name}'
                }
            }, {
                xtype: 'displayfield',
                name: 'username',
                label: 'Username',
                bind: {
                    value: '{selectedUser.username}'
                }
            }, {
                xtype: 'displayfield',
                name: 'email',
                label: 'Email',
                bind: {
                    value: '{selectedUser.email}'
                }
            }, {
                xtype: 'displayfield',
                name: 'phone',
                label: 'Phone',
                bind: {
                    value: '{selectedUser.phone}'
                }
            }]
        }]
    }, {
        extend: 'Ext.form.Panel',
        title: 'Address',
        flex: 1,
        margin: '40 10 40 0',
        items: [{
            xtype: 'fieldset',
            reference: 'fieldset1',
            items: [{
                xtype: 'displayfield',
                name: 'city',
                label: 'City',
                bind: {
                    value: '{selectedUser.address.city}'
                }
            },{
                xtype: 'displayfield',
                name: 'zipcode',
                label: 'Zipcode',
                bind: {
                    value: '{selectedUser.address.zipcode}'
                }
            }, {
                xtype: 'displayfield',
                name: 'street',
                label: 'Street',
                bind: {
                    value: '{selectedUser.address.street}'
                }
            }, {
                xtype: 'displayfield',
                name: 'suite',
                label: 'Suite',
                bind: {
                    value: '{selectedUser.address.suite}'
                }
            }, {
                xtype: 'displayfield',
                name: 'location',
                label: 'Location (lat/lng)',
                bind: {
                    value: '{selectedUser.address.geo.lat}/{selectedUser.address.geo.lng}'
                }
            }]
        }]
    }, {
        extend: 'Ext.form.Panel',
        title: 'Company',
        flex: 1,
        margin: '40 40 40 0',
        items: [{
            xtype: 'fieldset',
            reference: 'fieldset1',
            items: [{
                xtype: 'displayfield',
                name: 'name',
                label: 'Name',
                bind: {
                    value: '{selectedUser.company.name}'
                }
            },{
                xtype: 'displayfield',
                name: 'catchPhrase',
                label: 'Catch Phrase',
                bind: {
                    value: '{selectedUser.company.catchPhrase}'
                }
            }, {
                xtype: 'displayfield',
                name: 'bs',
                label: 'bs',
                bind: {
                    value: '{selectedUser.company.bs}'
                }
            }]
        }]
    }, {
        extend: 'Ext.pivot',
    }]
});