/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('UsersPics.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    requires: [
        'UsersPics.model.Personnel',
        'UsersPics.view.adduser.AddUser'
    ],

    onAddUser: function () {
        let usersGrid = Ext.getCmp('users-grid');
        let dialog = Ext.create({
            xtype: 'dialog',
            title: 'Add new User',
            closable: true,
            plugins: {
                responsive: true
            },
            responsiveConfig: {
                'width < 1000': {
                    minHeight: '95%',
                    minWidth: '95%',
                },
                'width >= 1000': {
                    minHeight: '60%',
                    minWidth: '50%',
                }
            },
            items: [{
               xtype: 'adduser',
            }],
            buttons: {
                submit: function () {
                    let formValues = Ext.getCmp('add-user').getValues();

                    let newUser = Ext.create('UsersPics.model.Personnel', formDataToStore(formValues));
                    newUser.save({
                        success: function() {
                            usersGrid.getStore().load()
                        }
                    });
                    dialog.destroy();
                }
            }
        });
        function formDataToStore (data) {
            return {
                name: data.name,
                email: data.email,
                phone: data.phone,
                company: {
                    name: data.companyName
                },
                address: {
                    city: data.addressCity,
                    street: data.addressStreet
                }
            }
        }
        dialog.show();
    }
});
