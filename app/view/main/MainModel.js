/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('UsersPics.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

});
