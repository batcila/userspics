// Main App view with included tabs

Ext.define('UsersPics.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.layout.Fit',
        'UsersPics.view.main.MainController',
        'UsersPics.view.main.MainModel',
        'UsersPics.view.pictures.Pictures',
        'UsersPics.view.users.UsersPanel'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        }
    },

    tabBarPosition: 'top',
    reference: 'main',
    items: [
        {
            title: 'Users',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
            items: [{
                xtype: 'userspanel'
            }]
        },{
            title: 'Pictures',
            iconCls: 'x-fa fa-image',
            layout: 'fit',
            items: [{
                xtype: 'pictures'
            }]
        }
    ]
});
