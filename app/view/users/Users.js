/**
 * Created by blabl on 4.10.2018 г..
 */
Ext.define('UsersPics.view.users.Users', {
    extend: 'Ext.grid.Grid',

    requires: [
        'Ext.grid.cell.Widget',
        'UsersPics.store.Personnel',
        'UsersPics.view.users.UsersController',
        'UsersPics.view.users.UsersModel'
    ],
    id: 'users-grid',
    xtype: 'users',
    viewModel: {
        type: 'users'
    },

    controller: 'users',

    store: {
        type: 'personnel',
        autoLoad: true,
    },

    columns: [{
        text: 'ID',
        dataIndex: 'id',
        cell: {
            userCls: 'bold',
        },
        flex: 'auto'
    }, {
        text: 'Name',
        dataIndex: 'name',
        renderer: function(value) {
            return value;
        },
        cell: {
            userCls: 'bold',
        },
        flex: 1,
        minWidth: '200px'
    }, {
        text: 'Email',
        dataIndex: 'email',
        renderer: function(value) {
            return value;
        },
        flex: 1,
        minWidth: '200px'
    }, {
        text: 'Phone',
        dataIndex: 'phone',
        renderer: function(value) {
            return value;
        },
        flex: 1,
        minWidth: '200px'
    }, {
        text: 'Address',
        dataIndex: 'address',
        renderer: function(value) {
            return value.street + ' street,' + value.city;
        },
        sorter: {
            // Sorting by Street name
            sorterFn: function (a,b){
                return ('' + a.data.address.street).localeCompare(b.data.address.street);
            }
        },
        flex: 2,
        minWidth: '300px'
    }, {
        text: 'Company',
        dataIndex: 'company',
        renderer: function (value) {
            return value.name;
        },
        sorter: {
            // Sorting by Company name
            sorterFn: function (a, b) {
                return ('' + a.data.company.name).localeCompare(b.data.company.name);
            }
        },
        flex: 1,
        minWidth: '200px'
    }, {
        text: 'Del',
        cell: {
            tools: [{
                iconCls: 'x-fa fa-user-times redIcon',
                handler: 'onDelUser'
            }],
        },
        flex: 'auto'
    }, {
        text: '',
        cell: {
            xtype: 'widgetcell',
            widget: {
                xtype: 'button',
                text: 'Show Info',
                dataIndex: this,
            },
        },
    }],
    listeners: {
        childdoubletap: {
            fn: 'onItemDblClick'
        },
        childtap: {
            fn: 'onItemClick'
        }
    }
});