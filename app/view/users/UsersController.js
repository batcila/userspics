/**
 * Created by blabl on 4.10.2018 г..
 */
Ext.define('UsersPics.view.users.UsersController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.users',
    requires: [
        'Ext.layout.Fit',
        'UsersPics.view.showuserinfo.ShowUserInfo'
    ],
    onItemClick: function (el, el2) {
        if (el2.sourceElement.type === 'button') {
            let rowData = el2.record.data;
            let mainMenu = this.getView().up('app-main');
            let tabExist = Ext.getCmp('rowID-' + el2.record.data.id);
            if (!tabExist) {
                let tab = mainMenu.add({
                    id: 'rowID-' + rowData.id,
                    closable: true,
                    title: rowData.name,
                    iconCls: 'x-fa fa-user',
                    layout: 'fit',
                    items: [{
                        xtype: 'showuserinfo',
                        selectedUser: rowData
                    }],
                });
                mainMenu.setActiveItem(tab)
            }
        }
    },
    onItemDblClick: function (el, extEl) {
        let usersGrid = extEl.view;
        let dialog = Ext.create({
            xtype: 'dialog',
            title: 'Edit User',
            closable: true,
            plugins: {
                responsive: true
            },
            responsiveConfig: {
                'width < 1000': {
                    minHeight: '95%',
                    minWidth: '95%',
                },
                'width >= 1000': {
                    minHeight: '60%',
                    minWidth: '50%',
                }
            },
            items: [{
                xtype: 'edituser',
                selectedUser: extEl.record.data,
            }],
            session: true,
            buttons: {
                submit: function () {
                    let formValues = Ext.getCmp('edit-user').getValues();
                    let userData = formDataToStore(formValues);
                    let user = extEl.record;
                    user.set(userData);
                    user.save({
                        success: function() {
                            usersGrid.getStore().load();
                        }
                    });
                    dialog.destroy();
                }
            }
        });
        function formDataToStore (data) {
            return {
                name: data.name,
                email: data.email,
                phone: data.phone,
                company: {
                    name: data.companyName
                },
                address: {
                    city: data.addressCity,
                    street: data.addressStreet
                }
            }
        }
        dialog.show();
    },
    onDelUser: function (el, extEl) {
        Ext.Msg.confirm("Delete User", "Are you sure you want to delete that user?", function(answer) {
            if (answer === 'yes') {
                extEl.record.erase()
            }
        });
    }
});