/**
 * Created by blabl on 4.10.2018 г..
 */
Ext.define('UsersPics.view.users.UsersPanel', {
    extend: 'Ext.Panel',

    xtype: 'userspanel',

    title: 'Users Panel',
    layout: 'fit',
    tools: [{
        iconCls: 'x-fa fa-plus',
        handler: 'onAddUser',
    }],

    requires: [
        'Ext.layout.Fit',
        'UsersPics.view.users.Users'
    ],

    items: [{
        xtype: 'users',
    }]

});