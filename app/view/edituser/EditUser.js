/**
 * Created by blabl on 5.10.2018 г..
 */
Ext.define('UsersPics.view.edituser.EditUser', {
    extend: 'Ext.form.Panel',
    id: 'edit-user',
    xtype: 'edituser',
    requires: [
        'Ext.field.Text',
        'Ext.form.FieldSet',
        'UsersPics.view.edituser.EditUserController',
        'UsersPics.view.edituser.EditUserModel'
    ],
    viewModel: {
        type: 'edituser'
    },

    controller: 'edituser',

    fullscreen: true,
    items: [
        {
            xtype: 'fieldset',
            items: [
                {
                    xtype: 'textfield',
                    name : 'name',
                    label: 'Name',
                    bind: {
                        value: '{selectedUser.name}'
                    }
                }, {
                    xtype: 'textfield',
                    name : 'email',
                    label: 'Email',
                    bind: {
                        value: '{selectedUser.email}'
                    }
                }, {
                    xtype: 'textfield',
                    name : 'phone',
                    label: 'Phone',
                    bind: {
                        value: '{selectedUser.phone}'
                    }
                }, {
                    xtype: 'textfield',
                    name : 'addressStreet',
                    label: 'Street',
                    bind: {
                        value: '{selectedUser.address.street}'
                    }
                }, {
                    xtype: 'textfield',
                    name : 'addressCity',
                    label: 'City',
                    bind: {
                        value: '{selectedUser.address.city}'
                    }
                }, {
                    xtype: 'textfield',
                    name : 'companyName',
                    label: 'Company',
                    bind: {
                        value: '{selectedUser.company.name}'
                    }
                }
            ]
        }
    ]
});