/**
 * Created by blabl on 5.10.2018 г..
 */
Ext.define('UsersPics.view.edituser.EditUserController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.edituser',

    /**
     * Called when the view is created
     */
    init: function() {
        let vm = this.getViewModel();
        vm.set('selectedUser', this.getView().selectedUser)
    }
});