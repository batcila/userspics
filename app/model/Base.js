Ext.define('UsersPics.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'UsersPics.model'
    }
});
