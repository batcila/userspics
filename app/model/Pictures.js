/**
 * Created by blabl on 4.10.2018 г..
 */
Ext.define('UsersPics.model.Pictures', {
    extend: 'Ext.data.Model',

    fields: [
        'title', 'url', 'thumbnailUrl'
    ]

});