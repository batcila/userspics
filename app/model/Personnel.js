Ext.define('UsersPics.model.Personnel', {
    extend: 'UsersPics.model.Base',

    requires: [
        'Ext.data.proxy.Rest',
        'Ext.data.writer.Json'
    ],

    fields: [
        'id', 'name', 'username', 'email', 'address', 'phone', 'website', 'company'
    ],

    proxy: {
        type: 'rest',
        url : 'http://batcila.me:3000/users',
        writer: {
            type: 'json',
            writeRecordId: false,
            writeAllFields: true
        }
    }
});
