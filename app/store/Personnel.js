Ext.define('UsersPics.store.Personnel', {
    extend: 'Ext.data.Store',

    alias: 'store.personnel',

    requires: [
        'Ext.data.proxy.Rest',
        'UsersPics.model.Personnel'
    ],

    model: 'UsersPics.model.Personnel',

    proxy: {
        type: 'rest',
        url : 'http://batcila.me:3000/users'
    }
});
