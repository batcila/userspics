Ext.define('UsersPics.store.Pictures', {
    extend: 'Ext.data.Store',

    alias: 'store.pictures',

    requires: [
        'Ext.data.proxy.Rest',
        'UsersPics.model.Pictures'
    ],

    model: 'UsersPics.model.Pictures',

    proxy: {
        type: 'rest',
        url : 'http://batcila.me:3000/pictures'
    }

});